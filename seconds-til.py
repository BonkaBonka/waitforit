#!/usr/bin/python3

import parsedatetime
import sys
import time

print(int(time.mktime(parsedatetime.Calendar().parse(sys.argv[1])[0]) - time.mktime(time.localtime())))
