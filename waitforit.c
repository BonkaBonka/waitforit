#include <getopt.h>
#include <libgen.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <hiredis/hiredis.h>

const char *subscribe_prefix = "SUBSCRIBE ";

char *redis_addr = "127.0.0.1";
int redis_port = 6379;
char *redis_channel = NULL;
unsigned timeout = -1;
bool quiet = false;

char *subscribe_command = NULL;
redisContext *ctx = NULL;

void cleanup() {
    if (subscribe_command) {
        free(subscribe_command);
        subscribe_command = NULL;
    }

    if (ctx) {
        if (ctx->err) {
            fprintf(stderr, "Redis Error: %s\n", ctx->errstr);
        }
        redisFree(ctx);
        ctx = NULL;
    }
}

void terminated_handler() {
    if (!quiet) {
        printf("terminated\n");
    }
    exit(1);
}

void alarm_handler() {
    if (!quiet) {
        printf("timeout\n");
    }
    exit(0);
}

void display_help(char *prog)
{
    fprintf(stderr, "Usage:\n");
    fprintf(stderr, "  %s [options] <seconds to wait>\n\n", basename(prog));
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "  -r, --redis-addr     redis server to connect to (default: %s)\n", redis_addr);
    fprintf(stderr, "  -p, --redis-addr     redis server port to use (default: %d)\n", redis_port);
    fprintf(stderr, "  -c, --redis-channel  redis pubsub channel\n");
    fprintf(stderr, "  -q, --quiet          do not output timeout or redis message\n");
    fprintf(stderr, "  -h, --help           display this help\n\n");
    fprintf(stderr, "Options:\n");
    fprintf(stderr, "  Waiting for 0 seconds waits indefinitely\n");
    fprintf(stderr, "  If no Redis channel then behave like sleep(1)\n\n");
}

int process_args(int argc, char **argv)
{
    static struct option long_options[] = {
        { "redis-addr",    required_argument, 0, 'r' },
        { "redis-port",    required_argument, 0, 'p' },
        { "redis-channel", required_argument, 0, 'c' },
        { "quiet",         no_argument,       0, 'q' },
        { "help",          no_argument,       0, 'h' },
        { NULL,            0,                 0, 0 }
    };

    int c;

    while ((c = getopt_long(argc, argv, "r:p:c:qh", long_options, NULL)) != -1) {
        switch (c) {
            case 'r':
                redis_addr = optarg;
                break;
            case 'p':
                redis_port = strtol(optarg, NULL, 10);
                break;
            case 'c':
                redis_channel = optarg;
                break;
            case 'q':
                quiet = true;
                break;
            default:
                display_help(argv[0]);
                return -1;
        }
    }

    if (!(optind == argc - 1)) {
        display_help(argv[0]);
        return -1;
    }

    timeout = strtoul(argv[optind], NULL, 10);

    return 0;
}

int main(int argc, char **argv) {
    atexit(cleanup);

    if (process_args(argc, argv) < 0) {
        exit(1);
    }

    struct sigaction action;
    action.sa_flags = 0;

    action.sa_handler = terminated_handler;
    sigemptyset(&action.sa_mask);
    sigaction(SIGHUP, &action, NULL);

    action.sa_handler = terminated_handler;
    sigemptyset(&action.sa_mask);
    sigaction(SIGQUIT, &action, NULL);

    action.sa_handler = terminated_handler;
    sigemptyset(&action.sa_mask);
    sigaction(SIGTERM, &action, NULL);

    action.sa_handler = terminated_handler;
    sigemptyset(&action.sa_mask);
    sigaction(SIGINT, &action, NULL);

    if (!redis_channel) {
        sleep(timeout);

        exit(0);
    }

    subscribe_command = malloc(1 + strlen(subscribe_prefix) + strlen(redis_channel));
    if (!subscribe_command) {
        fprintf(stderr, "Memory Error: Can't allocate subscribe command\n");
        exit(1);
    }

    memcpy(subscribe_command, subscribe_prefix, 1 + strlen(subscribe_prefix));
    strcat(subscribe_command, redis_channel);

    ctx = redisConnect(redis_addr, redis_port);
    if (!ctx) {
        fprintf(stderr, "Memory Error: Can't allocate redis context\n");
        exit(1);
    }
    if (ctx->err) {
        // Error message printed when context is being cleaned up
        exit(1);
    }

    action.sa_handler = alarm_handler;
    sigemptyset(&action.sa_mask);
    sigaction(SIGALRM, &action, NULL);

    alarm(timeout);

    redisReply *reply = redisCommand(ctx, subscribe_command);

    // Don't care what the reply is so just discard it
    freeReplyObject(reply);
    if (ctx->err) {
        // Error message printed when context is being cleaned up
        exit(1);
    }

    // Not sure why hiredis makes me cast the reply
    if (redisGetReply(ctx, (void **) &reply) == REDIS_OK) {
        if (reply->type == REDIS_REPLY_ARRAY) {
            if (reply->elements == 3) {
                if (strcasecmp("message", reply->element[0]->str) == 0) {
                    if (!quiet) {
                        printf("%s\n", reply->element[2]->str);
                    }
                } else {
                    fprintf(stderr, "Unhandled redis response: %s\n", reply->element[0]->str);
                    exit(1);
                }
            } else {
                fprintf(stderr, "Unhandled redis reply length: %ld\n", reply->elements);
                exit(1);
            }
        } else {
            fprintf(stderr, "Unhandled redis reply type: %d\n", reply->type);
            exit(1);
        }

        freeReplyObject(reply);
        if (ctx->err) {
            // Error message printed when context is being cleaned up
            exit(1);
        }
    } else {
        // Error message printed when context is being cleaned up
        exit(1);
    }

    exit(0);
}
