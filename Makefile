CFLAGS?=-O1 -Wall -pedantic
LDFLAGS?=-lhiredis
PREFIX?=/usr/local

BINS?=waitforit

%: %.c
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

all: $(BINS)

install: all
	install $(BINS) -s -D -t "$(PREFIX)/bin/"

clean:
	rm -f $(BINS)
